package example;

public class Main {
    /**
     * Die Main-Klasse ist hier nicht das game an sich. Wieso?
     * Eine Main-Klasse sollte niemals direkt das sein, was gestartet werden soll (hier das Spiel)
     * Angenommen wir hätten die main-methode einfach in die Game-Klasse gepackt, hätten wir ein Problem, sollte etwas schief gehen.
     * Des weiteren ist der try catch block nicht notwendig, aber hilft, wenn ein unerwarteter Fehler auftritt
     * @param args
     */
    public static void main(String[] args) {
        try {
            // Erstelle ein neues Spiel
            Game game = new Game();
            // Starte das Spiel
            game.start();
        } catch(Exception e) {
            // Hier kann ein sicheres Fallback-Fenster geöffnet werden, wo die Nachricht an den Server gesendet wird und dem Nutzer
            // gesagt wird, dass ein Fehler aufgetreten ist.
            System.out.println("$ The program encountered an unexpected error!");
            e.printStackTrace();
        }
    }
}