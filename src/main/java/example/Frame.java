package example;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.util.List;

import javax.swing.JFrame;

/**
 * Das hier ist das Haupt-Fenster. Ich habe es hier, der Einfachheit halber mit Swing gemacht.
 * Das Fenster hat eine Refferenz auf alle Komponenten des Spiels.
 * Es besorgt sich die Position und das aktuelle Bild der Komponenten, nachdem diese neu berechnet wurden.
 */
public class Frame extends JFrame{

    // Das hier sind die Komponenten, die gezeichnet werden müssen
    final Player player;
    final Background bg;
    private List<Bullet> bullets;
    private List<Enemy> enemys;

    // Die BufferStrategy wird etwas weiter unten erklärt
    private BufferStrategy strat;

    /**
     * Das Fenster erwartet beim erstellen die Refferenzen
     *
     * @param player Der aktuelle Spieler
     * @param bg Der Hintergrund
     * @param bullets Die Schüsse
     * @param enemys Die Gegner.
     */
    public Frame (Player player, Background bg, List<Bullet> bullets, List<Enemy> enemys) {
        // Wir geben dem JFrame unseren Titel
        super("Example Game");
        // Hier fügen wir unseren Tastatur-Keylistener hinzu.
        addKeyListener(new Keyboard());
        this.player = player;
        this.bg = bg;
        this.bullets = bullets;
        this.enemys = enemys;
    }

    /**
     * Die BufferStrategy ist ein mächtiger Algorithmuss.
     * Wenn wir Frames berechnen, kann es sein, dass wir so schnell berechnen, dass wir nicht mehr hinterherkommen diese auch an zu zeigen.
     * Das sorgt für ein Flackern auf dem Fenster. Dies kommt häufig bei zeichnen von Flammen, Rauch oder Wasser vor.
     * Die BufferStrategy schafft dabei abhilfe. Wir Zeichnen ein Bild und packen es in den buffer.
     * Werden nun zu viele Bilder gezeichnet, werden diese gespeichert.
     * Dann, wenn wir .show() unsere bufferStrategy nutzen, zeigen wir das nächste Bild in dem Buffer.
     * Dabei gibt es noch andere Sachen, wie den Page-Flipping-Algorithm oder andere Sub-Varianten der BufferStrategy.
     * Die BufferStrategy ist aber die am besten verständliche und wird, nativ, von JavaFX verwendet,
     * auch wenn man gucken muss, wie man das mit JavaFX genau macht.
     */
    public void makeStrat() {
        // Wir kreieren die Methode von JFrame um eine Bufferstrategy zu erstellen
        createBufferStrategy(2);
        // und holen und speichern uns diese.
        strat = getBufferStrategy();
    }

    /**
     * Diese Methode dient als Schnittstelle, um alles neu zu zeichnen
     * Zu erste holen wir uns die nächste Graphik aus dem buffer und lassen uns diese befüllen.
     * Da dies eine Refferenz ist, brauchen wir das nicht zurück geben.
     */
    public void repaintScreen() {
        // Hole die nächste graphic
        Graphics g = strat.getDrawGraphics();
        // Übergebe diese der zeichen-method
        draw(g);
        // lösche unsere lokale Variable um Speicher frei zu geben
        g.dispose();
        // Sage dem Buffer, er soll das nächste Bild anzeigen
        strat.show();
    }

    /**
     * Draw zeichnet die Bilder in das Fenster.
     * Diese ist private, damit keine andere Prozedur einfach irgendwas zeichnet.
     * @param g die Graphic, auf der gezeichnet werden soll.
     *          Hier arbeiten wir mit der Refferenz, damit nichts zurück gegeben werden muss.
     */
    private void draw(Graphics g) {
        g.setColor(Color.red);

        // Überall, wo null steht, erwartet die Methode drawImage einen ImageObserver.
        // Dieser kann genutzt werden, muss aber nicht. Deswegen gebe ich hier null rein.

        //Zeichne den Hintergrund
        g.drawImage(bg.getLook(), bg.getX(), 0, null);

        //Zeichne den Hintergrund ein zweites mal, damit er unendlich durchläuft
        // Sollte die rechte Seite des Hintergrundes zu weit rüber gucken, fangen wir wieder ganz links an zu zeichnen.
        g.drawImage(bg.getLook(), bg.getX() + bg.getLook().getWidth(), 0, null);

        //Zeichne die Gegner
        for(int i = 0 ; i < enemys.size() ; i++) {
            Enemy e = enemys.get(i);
            g.drawImage(e.getLook(), e.getBounding().x, e.getBounding().y, null);
        }

        //Zeichne die Schüsse
        for(int i = 0 ; i < bullets.size() ; i++) {
            Bullet b = bullets.get(i);
            g.drawImage(Bullet.getLook(), b.getBounding().x, b.getBounding().y, null);
        }

        //Zeichne den Spieler
        g.drawImage(player.getLook(), player.getBounding().x, player.getBounding().y, null);
    }


}