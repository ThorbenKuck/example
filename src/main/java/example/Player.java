package example;

import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;

/**
 * Der Player ist der Zentrale Spieler des Spiels. Hier ist es das Raumschiff.
 * Die Berechnung der Position des Spielers übernimmt der Spieler selbst.
 * Das ist einfacher, als es aus zu lagern.
 * <p>
 * Was aber auffällt ist, dass alle Komponenten eine weitere Abstraktionsebene vertragen können, eigentlich sogar 2
 * Aber das lasse ich hier der einfachheit halber mal weg.
 */
public class Player {
    private float fPlayer_pos_x;
    private float fPlayer_pos_y;
    private int iWorld_size_x;
    private int iWorld_size_y;
    private float timeSinceLastShoot = 0;
    private final float SHOOTFREQUENZY = 0.1f;
    private float time_to_restart = 0;

    private Rectangle bounding;
    private BufferedImage look;
    private BufferedImage look_dead;
    private List<Bullet> bullets;
    private List<Enemy> enemys;
    private boolean alive = true;

    /**
     * Der Player erwartet mehrere Sachen im Konstruktor, welche ausgelagert werden könnten.
     * Abermals: Ich bin faul und hatte keine Lust darauf, dass jetzt noch zu machen
     * Ich lade hier die Bilder aus dem Resource-folder. Unter src.main.resources liegen die Bilder.
     * Allerdings würde ich empfehlen, diese aus einem Ordner zu lesen. Das ist zwar einmal mehr Arbeit,
     * aber erleichtert später das Testen und debugen.
     *
     * @param x             Die start-x-position
     * @param y             Die start-y-position
     * @param iWorld_size_x selbst erklärend
     * @param iWorld_size_y selbst erklärend
     * @param bullets       Eine refferenz auf die Kugeln, damit die berechnung einfacher ist
     * @param enemys        Eine refferenz auf die Gegner, damit die berechnung einfacher ist
     */
    public Player(int x, int y, int iWorld_size_x, int iWorld_size_y, List<Bullet> bullets, List<Enemy> enemys) {
        // Hier laden wir die Bilder.
        // Ich habe hier getrost das fallback ignoriert.
        // Wenn eine Exception auftaucht, wird hier nicht recovered.
        try {
            look = ImageIO.read(getClass().getClassLoader().getResourceAsStream("raumschiffchen.png"));
            look_dead = ImageIO.read(getClass().getClassLoader().getResourceAsStream("raumschiff_kaputt.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Das Rectangle, welches hier gezeichnet wird, ist eine "Hitbox".
        // Diese wird für die Logik benötigt. Ansonsten nicht wirklich.
        // Wichtig ist zu verstehen, dass alles aus dreiecken besteht, auch das Viereck.
        this.bounding = new Rectangle(x, y, look.getWidth(), look.getHeight());
        this.fPlayer_pos_x = x;
        this.fPlayer_pos_y = y;

        // Wir setzten hier die Variablen.
        this.iWorld_size_x = iWorld_size_x;
        this.iWorld_size_y = iWorld_size_y;
        this.bullets = bullets;
        this.enemys = enemys;

    }

    /**
     * Das ist die Methode, die im Game-Loop in calc aufgerufen wird.
     * Das kännte ausgelagert werden in ein Interface und man könnte dann sagen, dass nur diese aktzeptiert werden.
     * Aber das muss nicht zwangsweise. Ist für gewöhnlich auch nur in Engines oder Frameworks der Fall.
     *
     * @param timeSinceLastFrame Die Zeit, die vergangen ist, seid dem letzten Zeichnen.
     */
    public void update(float timeSinceLastFrame) {

        // Wenn wir tot sind, berechnen wir nichts.
        if (!alive) return;

        // Die berechnung, ob wir schießen, machen wir abhängig von timeSinceLastFrame.
        this.timeSinceLastShoot += timeSinceLastFrame;
        // Alle Tastenanschläge überprüfen
        // Bewegung
        if (Keyboard.isKeyDown(KeyEvent.VK_W) || Keyboard.isKeyDown(KeyEvent.VK_UP))
            this.fPlayer_pos_y -= 300 * timeSinceLastFrame;
        if (Keyboard.isKeyDown(KeyEvent.VK_S) || Keyboard.isKeyDown(KeyEvent.VK_DOWN))
            this.fPlayer_pos_y += 300 * timeSinceLastFrame;
        if (Keyboard.isKeyDown(KeyEvent.VK_A) || Keyboard.isKeyDown(KeyEvent.VK_LEFT))
            this.fPlayer_pos_x -= 300 * timeSinceLastFrame;
        if (Keyboard.isKeyDown(KeyEvent.VK_D) || Keyboard.isKeyDown(KeyEvent.VK_RIGHT))
            this.fPlayer_pos_x += 300 * timeSinceLastFrame;
        // Schießen
        if (this.timeSinceLastShoot >= this.SHOOTFREQUENZY && Keyboard.isKeyDown(KeyEvent.VK_SPACE)) {
            bullets.add(new Bullet(fPlayer_pos_x + ((this.look.getWidth() / 2) - (Bullet.getLook().getWidth() / 2)), fPlayer_pos_y + ((this.look.getHeight() / 2) - (Bullet.getLook().getHeight() / 2)), 750, 0, bullets));
            this.timeSinceLastShoot = 0;
        }

        // Überprüfen, ob der Spieler theoretisch auserhalb des Fensters ist und falls das der Fall ist,
        // setze ihn zurück in das Fenster.
        // Spieler Links aus dem Bildschirm
        if (this.fPlayer_pos_x < 0) this.fPlayer_pos_x = 0;
        // Spieler Oben aus dem Bildschirm
        if (this.fPlayer_pos_y < 0) this.fPlayer_pos_y = 0;
        // Spieler Rechts aus dem Bildschirm
        if (this.fPlayer_pos_x > this.iWorld_size_x - this.bounding.width)
            this.fPlayer_pos_x = this.iWorld_size_x - this.bounding.width;
        // Spieler Unten aus dem Bildschirm
        if (this.fPlayer_pos_y > this.iWorld_size_y - this.bounding.height)
            this.fPlayer_pos_y = this.iWorld_size_y - this.bounding.height;

        // Nun überführe die neue Position auf die Hitbox
        this.bounding.x = (int) this.fPlayer_pos_x;
        this.bounding.y = (int) this.fPlayer_pos_y;

        // Hier führen wir die Überprüfung durch, ob uns ein Spieler berührt und wir "tot sind".
        // Wichtig ist, dass wir hier keine foreach-schleifen nutzen, um die ConcurrentModificationException zu verhindern
        for (int i = 0; i < enemys.size(); i++) {
            Enemy e = enemys.get(i);

            // Dadurch, dass wir die Rectangle-Klasse nutzen, können wir einfach abfragen, ob ein Spieler uns berührt,
            // indem wir die intersects-Methode nutzen.
            if (bounding.intersects(e.getBounding()) && e.isAlive()) {
                alive = false;
                this.time_to_restart = System.currentTimeMillis();
            }
        }
    }

    /**
     * Ein getter für die Hitbox
     * @return die Hitbox
     */
    public Rectangle getBounding() {
        return this.bounding;
    }

    /**
     * Ein getter für die aktuelle X-Position
     * @return player_pos_x
     */
    public float getX() {
        return (this.fPlayer_pos_x);
    }

    /**
     * Ein getter für die aktuelle Y-Position
     * @return play_pos_y
     */
    public float getY() {
        return (this.fPlayer_pos_y);
    }

    /**
     * Hier bekommen wir das tatsächliche Aussehen.
     * Recht simpel. Wir haben hier nur 2 Bilder, eines für "am leben" und eines für "tot".
     * Wie eine Bewegung funktioniert sieht man in der Enemy-Klasse.
     * Aber, hier könnte man auch Bilder berechnen. Muss man aber nicht.
     *
     * @return das Bild
     */
    public BufferedImage getLook() {
        if (alive) {
            return look;
        } else {
            return look_dead;
        }
    }

    /**
     * Sagt, ob das Spiel neu gestartet werden soll
     * @return boolean, ob neugestartet werden soll
     */
    public boolean restart() {
        return System.currentTimeMillis() - time_to_restart < 5000;
    }
}