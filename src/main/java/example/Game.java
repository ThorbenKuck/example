package example;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * Die Game-Klasse ist die "Haupt-Klasse" Wenn die Methode start aufgerufen wird, erstellt sie das Fenster und den main-loop.
 * Was der loop macht, ist weiter unten erklärt.
 * Es mag scheinen, dass der loop für unser Projekt (RoboRally) nicht notwendig ist, aber ohne Loop keinerlei Annimation
 * Es bedarf eines Loops, um Bewegung dar zu stellen.
 */
public class Game {

    // Die Meisten Variablen hier lasse ich vom Comiler erstellen.
    // Diese können auch in den Konstruktor gepackt werden, aber ich war zu faul... sry.

    // Die beiden private attribute beschreiben die "Auflösung" des Fensters
    // Wie man das genau mit der Auflösung macht, ist fiddelig und wird in der Klasse "ShowDisplayModes" erklärt.
    // Ich halte das hier aber einfach
    private int screenWidth = 800;
    private int screenHeight = 600;

    // Die Liste aller Schüsse
    private List<Bullet> bullets = new LinkedList<>();
    // Die Liste aller Gegner
    private List<Enemy> enemys = new LinkedList<>();
    // Ein neuer Spieler
    private Player player = new Player(300, 300, screenWidth, screenHeight, bullets, enemys);
    // Ein Hintergrund.
    private Background bg = new Background(50);
    // Ale benötigten Variablen
    private Frame frame;
    private long lastFrame;
    private final float ENEMY_SPAWNTIME = 0.7f;
    private float timeSinceLastFrame;
    private float timeSinceLastEnemySpawn;

    private static boolean gameRunning;

    private Random r = new Random();

    /**
     * Der Haupt-loop befindet sich hier.
     * Der besteht aus diesen 3 Methoden. Ziemlich einfach
     * init() erstellt alle notwendigen Abhängigkeiten.
     * while(gameRunning) ist effektiv eine while(true) schleife und wird auch gerne "Herz", oder "clock" genannt.
     * diese kann über die Variable gameRunning sauber beendet werden.
     * in der Schleife wird immer nach einander calc() + draw() ausgeführt!
     * Das sorgt dafür, dass Annimationen flüssig ausgeführt werden.
     * calc() sagt allen bestandteilen des Fensters (hier dem Spieler, den shüssen und den Gegnern), dass sie ihre Position neu berechnen sollen
     * und draw() zeichnet dann die neu berechneten Bestandteile des Spiels.
     * Ist das Spiel beendet, sagen wir einfach dass das Fenster geschlossen werden soll.
     */
    public void start() {

        // Kreiere die initialen abhängigkeiten
        init();

        while(gameRunning) {

            // Berechne die neuen Positionen
            calc();

            // Zeichne die bestandteile neu
            draw();

        }

        // Schließe das Fenster
        frame.setVisible(false);
        frame.dispose();
    }

    /**
     * hier kreieren wir simple die benötigten Sachen.
     * Erstellen das Fenster (hier mit Swing erstellt), setzen die größe usw.
     * Die beiden variablen timeSinceLastEnemySpawn und lastFrame werden noch wichtig in calc.
     */
    private void init() {
        // Spawne den ersten Gegner
        spawnEnemy();

        // Erstelle das Fenster und setze die benötigten Dinge
        frame = new Frame(player, bg, bullets, enemys);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(screenWidth, screenHeight);
        frame.setUndecorated(true);
        frame.setVisible(true);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.makeStrat();

        // Erstelle initial die benötigten Variablen
        timeSinceLastEnemySpawn = 0;
        lastFrame = System.currentTimeMillis();

        // Starte den game-loop
        gameRunning = true;
        System.out.println("$ Game has been started");
    }

    /**
     * Diese Methode berechnet mehrere Sachen. Zum ersten guckt sie hier, ob daas Spiel, durch drücken von "Esc" beendet werden soll.
     * Danach werden die "frames" berechnet. Folgendes ist dabei relevant:
     *
     * 1.  timeSinceLastFrame = Dies ist eine der notwendigsten variablen um eine flüssige Darstellung zu ermöglichen.
     *     Wenn ein Problem (lag) auftritt, sei es Hardware bedingt oder software bedingt (alt + tab produziert gerne lags)
     *     können die Komponenten darauf reagieren und trotzdem korrekt ihre Position berechnen.
     * 2.  lastFrame = Ist eine art "Zeit-Stempel", wie lange es her ist, seit dem letzten Frame
     * 3.  timeSinceLastEnemySpawn = Hier verwende ich noch diese Variable um zu kontrollieren, wie viele Gegner in wie viel Zeit spawnen.
     *
     * Danach sage ich dem Player, Hintergrund (bg), den schüssen (bullets) und den Gegnern (enemys), dass die sich anpassen sollen, relativ zu timeSinceLastFrame.
     *
     * Bei unserem Projekt würde Calc folgendes durchlaufen:
     *
     * 1. Gucke ob etwas vom Server kam (vielleicht in einer Sammel-Klasse)
     * 2. Wandle die Änderungen in tatsächliche Änderungen um.
     *    Z.B: Kommt vom Server, der Roboter1 bewegt sich auf Feld 2,2 sage dem Roboter1, das er zu Feld 2,2 will
     * 3. Lasse die Berechnung durchführen, wie ganz normal.
     *
     * Dann wird über die Zeit von X-Frames die neuberechnung und Zeichnung durch geführt.
     */
    private void calc() {

        // gucke ob neu gestartet werden muss
        checkForRestart();

        // Wenn Esc gedrückt ist, stoppe das Spiel.
        if(Keyboard.isKeyDown(KeyEvent.VK_ESCAPE)) gameRunning = false;

        // Berechne die benötigten Variablen
        long thisFrame = System.currentTimeMillis();
        timeSinceLastFrame = ((float)(thisFrame - lastFrame)) / 1000f;
        lastFrame = thisFrame;

        // Gucke, ob Gegner gespawned werden müssen
        timeSinceLastEnemySpawn += timeSinceLastFrame;
        if(timeSinceLastEnemySpawn >= ENEMY_SPAWNTIME) {
            timeSinceLastEnemySpawn -= ENEMY_SPAWNTIME;
            spawnEnemy();
        }

        // Dann sage allen, dass sie ihre Position neu berechnen sollen
        player.update(timeSinceLastFrame);

        bg.update(timeSinceLastFrame);

        for(int i = 0 ; i < bullets.size() ; i++) {
            bullets.get(i).update(timeSinceLastFrame);
        }

        for(int i = 0 ; i < enemys.size() ; i++) {
            enemys.get(i).update(timeSinceLastFrame);
        }
    }

    /**
     * Hier werden die gesamten komponenten neu gezeichnet. Dies geschieht in dem frame, da er die Instancen von allen besitzt.
     * Danach kommt ein Thread.sleep(). Warum? Wenn wir das nicht hätten, würden wir abhängig von der Geschwindigkeit der CPU,
     * unglaublich viele Frames pro sekunde haben. Ab ca. 1000 frames pro sekunde (fps) ist das so schnell, dass das Bild (gefühlt) in Lichtgeschwindigkeit gezeichnet wird.
     * Die Zeit von 15 millisekunden ist meiner Meinung nach ganz gut.
     * Ich nutze hier Thread.sleep aus dem Grund, dass wir das interrupten können, aber tatsächlich kann hier alles genutzt werden.
     * Die einzige bedingung ist, dass das Programm dadurch angehalten wird.
     */
    private void draw() {

        // Zeichne das Fenster neu
        frame.repaintScreen();

        try {
            Thread.sleep(15);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Hier fügen wir einfach einen neuen Gegner der Gegner-Liste hinzu.
     */
    private void spawnEnemy() {
        // Die ersten beiden Parameter, die hier übergeben werden, beschreiben ja den SpawnPoint.
        // Hier kann man natürlich absolute Werte einfügen (auch wenn das nur selten gut ist).
        // Ich sage hier, spawne am rechtesten Rand und random auf der Höhe.
        enemys.add(new Enemy(screenWidth - Enemy.getWidth(), r.nextInt(screenHeight - Enemy.getHeight()), bullets, player));
    }

    /**
     * Wenn der Spieler tot ist (getroffen von einem Gegner) wird:
     * 1. Das Fenster geschlossen
     * 2. Alle Komponenten neu erstellt
     * 3. Das Fenster neu erstellt
     * die restart methode des Players kann erweitert werden, auf irgend einen Knopfdruck, oder sowas.
     */
    private void checkForRestart() {
        if(player.restart()) {
            frame.setVisible(false);
            frame.dispose();
            System.out.println("$ Spiel wurde neu gestartet");
            bullets = new LinkedList<>();
            enemys = new LinkedList<>();
            player = new Player(300, 300, screenWidth, screenHeight, bullets, enemys);
            frame = new Frame(player, bg, bullets, enemys);

            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setSize(screenWidth, screenHeight);
            frame.setUndecorated(true);
            frame.setVisible(true);
            frame.setResizable(false);
            frame.setLocationRelativeTo(null);

            frame.makeStrat();
            spawnEnemy();
        }
    }
}
