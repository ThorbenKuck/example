package example;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;
import java.util.Random;

import javax.imageio.ImageIO;

/**
 * Der Gegner ist ein, von einer sehr simplen KI gesteuerter bösewicht.
 * Er ist ebenfalls eine Komponente und berechnet sein Position und sein Bild selbst
 */
public class Enemy {
    // Hier haben wir jetz nicht ein Bild, wie bei dem Player, sondern 4
    // Ansonsten ist das ganze recht simple
    private static BufferedImage[] look = new BufferedImage[4];
    private static BufferedImage look_dead;
    private final static float NEEDEDANITIME = 0.4f;
    private final static Random r = new Random();
    private float aniTime = 0;
    private float fPos_x;
    private float fPos_y;
    private long time_dead;
    private Rectangle bounding;
    private List<Bullet> bullets;

    private boolean alive = true;
    private Player player;

    // Hier überlasse ich dem Compiler die zuführung der Bilder. Das hat vor und nachteile.
    // Das kann zu nasty Exceptions führen, aber ich bin faul.
    static {
        try {
            look[0] = ImageIO.read(Bullet.class.getClassLoader().getResourceAsStream("enemy1.png"));
            look[1] = ImageIO.read(Bullet.class.getClassLoader().getResourceAsStream("enemy2.png"));
            look[2] = ImageIO.read(Bullet.class.getClassLoader().getResourceAsStream("enemy3.png"));
            look[3] = look[1];
            look_dead = ImageIO.read(Bullet.class.getClassLoader().getResourceAsStream("enemy_kaputt.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Beim erstellen eines Gegners mache ich so ziemlich alles, was ich sonst auch mache (z.b. beim Spieler).
     * @param x die x-position
     * @param y die y-position
     * @param bullets eine Refferenz auf die Schüsse
     * @param player eine Refferenz auf den Spieler
     */
    public Enemy(float x, float y, List<Bullet> bullets, Player player) {
        this.fPos_x = x;
        this.fPos_y = y;
        this.bounding = new Rectangle((int) x , (int) y , look[0].getWidth() , look[0].getHeight());
        this.bullets = bullets;
        this.player = player;
    }

    /**
     * Die update-Methode die von calc aufgerufen wird.
     * @param timeSinceLastFrame ja.. das habe ich jetzt schon mehrmals erklärt..
     */
    public void update(float timeSinceLastFrame) {

        // aniTime wird dazu genutzt, um das Bild zu berechnen, welches Bild angezeigt werden soll
        // Das ist die "Animation" der Gegner
        this.aniTime += timeSinceLastFrame;
        if(this.aniTime > NEEDEDANITIME) this.aniTime = 0;

        // Das ist die simple KI.
        // Ein Gegner guckt einfach wo der Spieler ist
        // und bewegt sich in seine Richtung
        // Dabei kann er sich in 8 Richtungen bewegen (auch schräg)
        if(this.alive) {
            //Oben
            if(player.getY() - this.fPos_y < -10) {
                this.fPos_y -= 100 * timeSinceLastFrame;
            }

            //Unten
            else if(player.getY() - this.fPos_y > 10) {
                this.fPos_y += 100 * timeSinceLastFrame;
            }

            //Links
            if(player.getX() < this.fPos_x) {
                this.fPos_x -= 100 * timeSinceLastFrame;
            }

            //Rechts
            else if(player.getX() > this.fPos_x) {
                this.fPos_x += 100 * timeSinceLastFrame;
            }
        }

        // Das ist der "De-Spawn", wenn der Gegner tot ist.
        // Dabei wird er dann neu erstellt.
        if(System.currentTimeMillis() - time_dead > 5000 && !alive) {
            this.fPos_x = 800;
            this.fPos_y = r.nextInt(600-getLook().getHeight());
            alive = true;
        }
        // Hier wird abgefragt, ob eine Kugel den Gegner trifft.
        // Abermals nutzen wir die intersects Methode der Rectangle-Klasse
        for(int i = 0 ; i < bullets.size() ; i++) {
            Bullet b = bullets.get(i);
            if(bounding.intersects(b.getBounding()) && alive) {
                alive = false;
                bullets.remove(b);
                time_dead = System.currentTimeMillis();
            }
        }

        // Und dann berechnen wir die neue Hitbox
        this.bounding.x = (int) this.fPos_x;
        this.bounding.y = (int) this.fPos_y;
    }

    // Habe ich schon 1000 mal erklärt...
    public Rectangle getBounding() {
        return bounding;
    }

    /**
     * Hier wird das Bild berechnet.
     * Abhängig von aniTime wird dabei eines der 4 Bilder gewählt in der folgenden Reihenfolge:
     * 1. Enemy1
     * 2. Enemy2
     * 3. Enemy3
     * 4. Enemy2
     * 5. repeat
     * zumindest, solange dieser Lebt
     * @return das Bild
     */
    public BufferedImage getLook() {
        if(!alive) return look_dead;
        else {
            if(look.length == 0) return null;
            for(int i = 0 ; i < look.length ; i++) {
                if(aniTime < NEEDEDANITIME / look.length * (i + 1)) {
                    return look[i];
                }
            }
            return look[look.length - 1];
        }
    }

    // Hier folgen nur ein paar getter.. Ich bin faul.. sry.

    public boolean isAlive() {
        return alive;
    }

    public static int getWidth() {
        return look[0].getWidth();
    }

    public static int getHeight() {
        return look[0].getHeight();
    }
}