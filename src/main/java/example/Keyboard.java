package example;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Das hier ist ein einfacher KeyListener für die Tastatur.
 */
public class Keyboard implements KeyListener{

    private static boolean[] keys = new boolean[1024];

    // Ich weiss nicht, was hier rein soll.
    // Abermals habe ich die instanziierung dem Compiler überlassen.
    public Keyboard() {
    }

    /**
     * Beschreibt, ob eine Teste gedrückt ist.
     * @param keyCode die KeyCodes findet man hier: https://docs.oracle.com/javase/7/docs/api/java/awt/event/KeyEvent.html
     *                Diese beginnen mit VK_ (z.b. VK_ENTER für die Enter-Taste oder VK_DOWN für Pfeiltaste nach unten)
     * @return true, wenn diese gedrückt ist.
     */
    public static boolean isKeyDown(int keyCode) {
        if(keyCode >= 0 && keyCode < keys.length) return keys[keyCode];
        else return false;
    }

    /**
     * Wennn eine Taste gedrückt wrid, setzte den keyCode auf true
     * Kommt von dem Interface KeyListener
     * @param e das KeyEvent
     */
    @Override
    public void keyPressed(KeyEvent e) {
        int keyCode = e.getKeyCode();
        if(keyCode >= 0 && keyCode < keys.length) keys[keyCode] = true;
    }

    /**
     * Wenn eine Taste los gelassen wird, setze den keyCode auf false
     * Kommt von dem Interface KeyListener
     * @param e das KeyEvent
     */
    @Override
    public void keyReleased(KeyEvent e) {
        int keyCode = e.getKeyCode();
        if(keyCode >= 0 && keyCode < keys.length) keys[keyCode] = false;
    }

















    // Wird nicht benötigt, muss aber implementiert werden!
    // Es beschreibt, wenn eine taste schnell runter gedrückt und sofort wieder los gelassen wird.
    // Ist in einem Spiel eigentlich nicht notwendig
    @Override
    public void keyTyped(KeyEvent e) {}

}