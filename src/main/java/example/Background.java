package example;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 * Der Hintergrund des Spiels. Er ist ebenfalls eine Komponente.
 * Hier habe ich eine Sache weg gelassen. Wenn man die Größe des Fensters ändert, kommt es dazu, dass unten ein weißer Balken kommt.
 * Das heißt, die Größe ist absolut und nicht relativ.
 */
public class Background {
    private float f_pos_x;
    private float f_speed;
    private BufferedImage look;

    /**
     * Der Hintegrund erwartet die Geschwindigkeit, in der er sich bewegen soll.
     * Das erzeugt die Illusion, dass das Raumschiff (der Player) sich mit einer konstanten Geschwindigkeit bewegt.
     * @param f_speed die Geschwindigkeit, mit der sich der Hintergrund bewegen soll.
     */
    public Background(float f_speed) {
        this.f_speed = f_speed;

        try {
            look = ImageIO.read(getClass().getClassLoader().getResourceAsStream("weltraum.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Hier schieben wir den Hintergrund einfach nur weiter.
     * @param timeSinceLastFrame
     */
    public void update(float timeSinceLastFrame) {
        this.f_pos_x -= this.f_speed * timeSinceLastFrame;
        if(this.f_pos_x < - look.getWidth()) this.f_pos_x += look.getWidth();
    }

    // Hier folgen jetzt abermals die Schnittstellen zum Zeichnen
    // Ich habe das schon erklärt und bin Faul, deswegen schreibe ich das nicht nochmal
    // Sry.
    public int getX() {
        return (int)f_pos_x;
    }

    public BufferedImage getLook() {
        return look;
    }
}