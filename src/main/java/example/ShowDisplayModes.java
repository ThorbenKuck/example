package example;

import java.awt.*;

public class ShowDisplayModes {
    public static void main(String[] args) {

//		DisplayMode displayMode = new DisplayMode(screenWidth, screenHeight, 16, 75);
        GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice device = env.getDefaultScreenDevice();

        DisplayMode[] ds = device.getDisplayModes();

        // Das was wir hier ausgeben, sind die möglichen Auflösungen.
        // Diese können in einem Menü angeboten und in den Settings gespeichert werden.
        for (DisplayMode d : ds) {
            System.out.println(d.getWidth() + " " + d.getHeight() + " " + d.getBitDepth() + " " + d.getRefreshRate());
        }

//		device.setFullScreenWindow(f);
//		device.setDisplayMode(displayMode);

    }
}
