package example;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;

/**
 * Eine Bullet ist ein Schuss des Spielers und außerdem eine Komponente
 */
public class Bullet {

    private static BufferedImage look;

    // Ich überlasse auch hier die instanziierung dem Compiler.
    static {
        try {
            look = ImageIO.read(Bullet.class.getClassLoader().getResourceAsStream("schuss.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private float f_position_x;
    private float f_position_y;
    private float f_speed_x;
    private float f_speed_y;
    private float fBullet_time_alive;
    private final float TIME_TO_LIVE = 10;

    private Rectangle bounding;
    private List<Bullet> bullets;

    /**
     * Blablabla erstellung...
     * Das ist die Letzte Klasse, die ich kommentiere, ich erkläre das nicht noch einmal
     * @param x
     * @param y
     * @param speedX
     * @param speedY
     * @param bullets
     */
    public Bullet( float x , float y , float speedX , float speedY , List<Bullet> bullets) {
        this.f_position_x = x;
        this.f_position_y = y;
        this.f_speed_x = speedX;
        this.f_speed_y = speedY;
        bounding = new Rectangle((int) x, (int) y, look.getWidth(), look.getHeight());
        this.bullets = bullets;
    }

    /**
     * Hier ist es recht simple. Die Kugel muss irgendwann gelöscht werden.
     * Warum? Wenn wir das nicht machen, haben wir irgendwann eine unendlich große Anzahl an Kugeln im Ram.
     * Deswegen geben wir denen eine "TIME_TO_LIVE". Wir könnten aber auch sagen, dass die gelöscht werden,
     * wenn die eine bestimmte weite erreichen.
     * @param timeSinceLastFrame blablabla
     */
    public void update(float timeSinceLastFrame) {
        this.fBullet_time_alive += timeSinceLastFrame;
        if(this.fBullet_time_alive > TIME_TO_LIVE) {
            bullets.remove(this);
        }
        this.f_position_x += this.f_speed_x * timeSinceLastFrame;
        this.f_position_y += this.f_speed_y * timeSinceLastFrame;
        bounding.x = (int) f_position_x;
        bounding.y = (int) f_position_y;
    }

    // Abermals ein paar getter

    public Rectangle getBounding() {
        return bounding;
    }

    public static BufferedImage getLook() {
        return look;
    }
}